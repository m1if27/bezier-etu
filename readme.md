# TP Bézier

Le but de ce TP est de travailler sur les courbes de Bézier. Ces courbes sont
très répandues pour décrire et manipuler des courbes car elles permettent un
contrôle simple et intuitif. Nous travaillerons ici avec des courbes de Bézier
cubiques, c'est à dire que le polynôme sous-jacent est de degré 3.

### Mise en place

Dans votre arborescence de 
[gKit3](https://forge.univ-lyon1.fr/JEAN-CLAUDE.IEHL/gkit3)
naviguez dans le dossier `projets` et clonez ce dépôt. Éditez ensuite le fichier
`premake4.lua` à la racine de gKit3 en ajoutant à la fin les lignes suivantes :

```lua
project("bezier")
  language "C++"
  kind "ConsoleApp"
  targetdir "bin"
  buildoptions ( "-std=c++17" )
  files ( gkit_files )
  files { "projets/bezier-etu/src/*.cpp" }
```

Générez ensuite les fichiers de compilation (voir le readme de gKit3) par
exemple pour linux + Makefile via la commande

```
premake5 gmake
```

La compilation de gKit génèrera désormais un éxécutable `bezier` de plus dans le
dossier `bin`. Lancez cet exécutable pour réaliser vos tests. Il génèrera des
images dans le dossier courant.

### Premier affichage de la courbe

Étant donné quatre points de contrôle $\mathbf{p_0}$, $\mathbf{p_1}$,
$\mathbf{p_2}$ et $\mathbf{p_3}$, la courbe est définie de façon paramétrique à
l'aide de quatre fonctions de base :

$$
\begin{aligned}
    B_0(t) &= (1-t)^3 \\
    B_1(t) &= 3t(1-t)^2 \\
    B_2(t) &= 3t^2(1-t) \\
    B_3(t) &= t^3 \\
\end{aligned}
$$

Pour chaque $t \in [0,1]$, un point de la courbe est défini comme

$$
    \mathbf{p}(t) =
        \mathbf{p_0}B_0(t)
        + \mathbf{p_1}B_1(t)
        + \mathbf{p_2}B_2(t)
        + \mathbf{p_3}B_3(t)
$$

Dans la paire de fichiers `bezier`, une structure de données est fournie
contenant un tableau de quatre points de contrôle. Implémentez la fonction
`interpolate` appliquant les formules ci-dessus pour calculer des points sur la
courbe. Activez ensuite le premier test en décommentant la ligne suivante au
début du ficher `main.cpp`

```cpp
#define PARAMETRIC_TEST
```

Si tout se passe bien vous devriez obtenir l'image suivante :

![resultat test parametrique](img/parametric.svg)

Vous pouvez modifier à la main les positions des points de contrôle dans le
fichier `main.cpp` pour expérimenter avec la forme de la courbe.

### Découpage de la courbe

Sur l'image précédente, vous pouvez remarquer que la courbe de Bézier passe par
deux de ses points de contrôle, $\mathbf{p_0}$ et $\mathbf{p_3}$. Les deux
autres points de contrôle permettent de contrôler les tangentes de la courbe aux
points $\mathbf{p}(0) = \mathbf{p_0}$ et $\mathbf{p}(1) = \mathbf{p_3}$. Un
outil très utiles sur les courbes de Bézier mis au point par [De
Casteljau](https://en.wikipedia.org/wiki/De_Casteljau%27s_algorithm) est l'ajout
de point de contrôles. Cet ajout se fait en découpant la courbe en deux
tronçons. Un nouveau point de contrôle est ajouté au point de jonction, et deux
autres sont ajoutés pour contrôler les tangentes de part et d'autre. Les
positions de ces trois points de contrôle sont calculées de telle sorte que la
courbe totale ne soit pas modifiée, et les points de contrôle $\mathbf{p_1}$ et
$\mathbf{p_2}$ sont déplacés.

Pour une courbe de Bézier contrôlée par les points $\mathbf{p_0}$, $\mathbf{p_1}$,
$\mathbf{p_2}$ et $\mathbf{p_3}$, et étant donné une valeur $t \in [0,1]$ telle
que $\mathbf{p}(t)$ est le point de découpage de la courbe, les nouveaux points
de contrôle sont calculés comme suit :

$$
\begin{aligned}
    \mathbf{p_{00t}} &= (1-t)\mathbf{p_0} + t\mathbf{p_1} \\
    \mathbf{p_{0t1}} &= (1-t)\mathbf{p_1} + t\mathbf{p_2} \\
    \mathbf{p_{t11}} &= (1-t)\mathbf{p_2} + t\mathbf{p_3} \\
    \mathbf{p_{0tt}} &= (1-t)\mathbf{p_{00t}} + t\mathbf{p_{0t1}} \\
    \mathbf{p_{tt1}} &= (1-t)\mathbf{p_{0t1}} + t\mathbf{p_{t11}} \\
    \mathbf{p_{ttt}} &= (1-t)\mathbf{p_{0tt}} + t\mathbf{p_{tt1}} \\
\end{aligned}
$$

Les deux portions de courbes résultantes sont alors respectivement définies par
les points de contrôle 

$$
\left\{
    \mathbf{p_0},
    \mathbf{p_{00t}},
    \mathbf{p_{0tt}},
    \mathbf{p_{ttt}}
\right\}
\quad \mathrm{et} \quad
\left\{
    \mathbf{p_{ttt}},
    \mathbf{p_{tt1}},
    \mathbf{p_{t11}},
    \mathbf{p_{3}}
\right\}
$$

Implémentez ce découpage via la méthode `split` de la classe `Bezier`. Testez
ensuite votre implémentation en décommentant la ligne

```cpp
#define SPLIT_TEST
```

Si tout se passe bien vous devriez obtenir l'image suivante :

![resultat test split](img/split.svg)

### Distance à une courbe de Bézier

Un problème intéressant est le calcul de la distance d'un point quelconque à une
courbe de Bézier. La courbe de Bézier étant un polynôme de degré 3, minimiser la
distance d'un point à ce polynôme revient à chercher les extrema d'un polynôme
de degré 6, et donc les racines d'un polynôme de degré 5, [ce qui n'est pas
trivial](https://en.wikipedia.org/wiki/Abel%E2%80%93Ruffini_theorem). Ci dessous
par exemple, un cas pour lequel la distance a 5 extrema.

![5 extrema pour la distance point bezier](img/bezier_point_distance.gif)

Nous allons donc calculer une valeur approchée de cette distance en utilisant
une méthode numérique. L'idée consiste à subdiviser la courbe en morceaux,
calculer des bornes sur la distance entre le point et chaque morceaux, et
éliminer les morceaux trop éloignés. Ce procédé raffine progressivement les
portions de courbes proches du point jusqu'à ce que l'incertitude sur la
distance soit plus petite qu'une tolérance donnée.

#### Borne supérieure sur la distance point courbe

Chaque point d'une courbe de Bézier est une combinaison linéaire des points de
contrôles, et les coefficients (donnés par les fonctions $B_i$) sont tous
positifs et de somme 1. On parle alors de combinaison convexe, et cette
propriété assure que tous les points de la courbe sont situés dans *l'enveloppe
convexe* des points de contrôle. Cette enveloppe convexe est le plus petit
polygone convexe (au sens de l'inclusion) contenant tous les points de contrôle.
Un polygone est convexe si pour toute paire de point contenus dans le polygone,
le segment joignant ces points est également contenu dans le polygone. Le fait
que la courbe soit dans cette enveloppe convexe signifie que tout ensemble
convexe contenant tous les points de contrôle contient aussi la courbe.

Plus particulièrement, si $\mathbf{q}$ est un point quelconque, nous cherchons
une distance $d$ telle que tout point de la courbe de Bézier soit plus proche de
$\mathbf{q}$ que cette distance. Autrement dit, la boule centrée en $\mathbf{q}$
de rayon $d$ contient totalement la courbe. Une boule est un ensemble convexe,
il suffit donc de trouver un $d$ tel que cette boule contienne tous les points
de contrôle de la courbe :

![majorant distance](img/sup_distance.svg)

Implémentez la méthode `sup_distance` de la classe `Bezier`. Une fois réalisée,
vous pouvez décommenter la ligne

```cpp
#define SUP_TEST
```

Vous devriez obtenir une image similaire à l'image ci-dessus.

#### Borne inférieure sur la distance point courbe

Pour calculer une borne inférieure par contre il ne suffit pas de trouver une
boule ne contenant aucun point de contrôle. On le constate par exemple sur 
l'animation ci-dessus illustrant la distance point courbe. L'idée ici consiste à
trouver une droite telle que la courbe soit totalement d'un côté, et la requête
de l'autre. La distance de la requête à cette droite est alors plus petite que
la distance de la requête à la courbe, car la plus grosse boule centrée sur la
requête ne traversant pas la droite a ce rayon, et vu qu'elle ne traverse pas la
droite, elle ne peut pas contenir de point de la courbe.

![idée minorant distance](img/principe_borne_inf.svg)

Cette droite n'existe cependant pas toujours, ou n'est pas forcément unique, et
il faut en choisir une intéressante. Nous vous proposons la méthode suivante :

* calculer le point $\mathbf{m} = \frac{1}{2}(\mathbf{p_0 + \mathbf{p_3}})$
* projeter tous les points de contrôle orthogonalement sur la droite $(\mathbf{q}, \mathbf{m})$
* déterminer le segment minimal de la droite contenant toutes les projections

La distance entre $\mathbf{q}$ et l'intervalle est notre borne inférieure (nulle
si $\mathbf{q}$ est dans l'intervalle).

![projection sur droite](img/algo_borne_inf.svg)

Implémentez la méthode `inf_distance` de la classe `Bezier`. Vous pourrez alors
décommenter le test

```cpp
#define INF_TEST
```

Vous devriez normalement obtenir l'image suivante

![minorant distance](img/inf_distance.svg)

#### Subdivision itérative de la courbe et élimination des portions inutiles

Munis de nos bornes, nous pouvons maintenant passer au découpage de la courbe.Le
principe consiste à maintenir une liste de portion de courbe candidates.
Initialement cette liste contient la courbe entière. À chaque itération toutes
les portions de courbe candidates seront coupées en deux nouvelles candidates au
paramètre `0.5`. Pour chaque portion candidate résultante, nous calculons les
bornes inférieure et supérieure. La borne inférieure globale de la distance
minimale est le minimum de toutes les bornes inférieures, et la borne supérieure
globale est le minimum de toutes les bornes supérieures. Pour chaque portion
candidate, si sa borne inférieure est plus grande que la borne supérieure
globale, la portion peut être éliminée.

![subdivision de la courbe](img/elimination_portion.svg)

Lorsque la différence entre les deux bornes globales est plus petite que la
précision souhaitée, les itérations s'arrêtent. Il est alors possible de
parcourir toutes les portions encore candidates et d'extraire parmis leurs
points de contrôle $\mathbf{p_0}$ et $\mathbf{p_3}$ celui qui est le plus proche
de la requête.

Implémentez ce mécanisme dans la fonction `nearest` de la classe `Bezier`. Cette
fonction retourne le $t \in [0,1]$ tel que le point $\mathbf{p}(t)$ de la courbe
de Bézier est le point le plus proche recherché. Vous pouvez alors décommenter
la ligne


```cpp
#define NEAREST_TEST
```

Vous devriez obtenir l'image suivante

![point le plus proche](img/nearest.svg)

## Une carotte pour aller plus loin

Vous trouverez dans les fichier `bezier` la définition d'une classe `Hair` qui
propose de rajouter une épaisseur variable à la courbe de Bézier : deux rayons
$r_0$ et $r_1$ sont fournis, et à chaque point $\mathbf{p}(t)$ de la courbe de
Bézier, nous attribuons le rayon $r(t) = (1-t)r_0 + tr_1$. La courbe épaissie
est l'union des boules centrées en $\mathbf{p}(t)$ de rayon $r(t)$ pour $t \in
[0,1]$. À vous de jouer pour étendre le travail précédent à ce nouvel objet en
implémentant ses bornes et sa fonction `nearest`. Dans le cas où la requête
$\mathbf{q}$ se trouverait à l'intérieur de la courbe épaissie, vous pouvez
retourner n'importe quel $t$ tel que 
$\lVert \mathbf{p}(t) - \mathbf{q} \rVert < r(t)$. Une fois ce travail réalisez,
décommentez le test

```cpp
#define CAROT_TEST
```

Vous devriez alors obtenir l'image suivante

![carotte](img/carot.svg)

## S'il vous reste de l'énergie

Pourquoi ne pas réaliser l'intersection avec un rayon, et tenter d'intégrer ces
courbes comme primitives dans votre petit lanceur de rayons personnel pour faire
des cheveux ou des plantes ?

