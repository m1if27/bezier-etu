#include "svg.h"
#include "bezier.h"

#include "vec.h"
#include "color.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <cassert>
#include <limits>
#include <cmath>
#include <algorithm>
#include <random>

using clk = std::chrono::high_resolution_clock ;
using time_point = clk::time_point ;

//===== Running tests =====

//comment to activate / deactivate tests

//#define PARAMETRIC_TEST
//#define SPLIT_TEST
//#define SUP_TEST
//#define INF_TEST
//#define NEAREST_TEST
//#define CAROT_TEST

//===== Utilities for plotting =====

static constexpr double res = 256 ;
static constexpr double pt_rad = 2 ;
static constexpr const char* stroke_width = "2" ;

std::string color_string(const Color& color) {
  std::stringstream ss ;
  ss << "rgb(" 
    << (int) (color.r * 256) << ","
    << (int) (color.g * 256) << ","
    << (int) (color.b * 256) << ")" ;
  return ss.str() ;
}

SVG::Circle* plot_point(
    SVG::Element* parent, 
    const Point& point, 
    double size = 1
    ) {
  SVG::Circle* c = parent->add_child<SVG::Circle>(
      res * point.x, 
      -res * point.y, 
      size*pt_rad
      ) ;
  c->set_attr("stroke-width", stroke_width) ;
  return c ;
}

SVG::Circle* plot_circle(
    SVG::Element* parent, 
    const Point& point, 
    double radius
    ) {
  SVG::Circle* c = parent->add_child<SVG::Circle>(
      res * point.x, 
      -res * point.y, 
      res*radius
      ) ;
  c->set_attr("fill", "none") ;
  c->set_attr("stroke", "black") ;
  c->set_attr("stroke-width", stroke_width) ;
  return c ;
}

SVG::Line* plot_segment(
    SVG::Element* parent, 
    const Point& from, 
    const Point& to
    ) {
  SVG::Line* l = parent->add_child<SVG::Line>(
      res * from.x, 
      res * to.x, 
      -res * from.y, 
      -res * to.y
      ) ;
  l->set_attr("stroke", "black") ;
  l->set_attr("stroke-width", stroke_width) ;
  return l ;
}


//===== Testing =====

int main() {

  //basic patch
  
  Bezier b(
    Point( 0., 0., 0.),
    Point( 1., 1., 0.),
    Point( 2.,-1., 0.),
    Point( 3., 1., 0.)
    ) ;

  std::vector<Point> curve ;

  int size = 100 ;
  for(int i = 0; i < size; ++i) {
    curve.emplace_back(b.interpolate((float) i / (float) (size-1))) ;
  }


#ifdef PARAMETRIC_TEST
  {
    //plot
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    //control points
    for(int i =0; i < 4; ++i) {
      SVG::Element* p = plot_point(grp, b.cp[i], 2) ;
      p->set_attr("fill", "black") ;
    }

    plot_segment(grp, b.cp[0], b.cp[1]) ;
    plot_segment(grp, b.cp[2], b.cp[3]) ;

    //curve points
    for(const Point& p : curve) {
      SVG::Element* c = plot_point(grp, p) ;
      c->set_attr("fill", "black") ;
      c->set_attr("stroke", "none") ;
    }

    //export the final svg
    svg.autoscale() ;
    std::ofstream file("parametric.svg") ;
    file << std::string(svg) ;

    std::cout << "parametric test generated in parametric.svg" << std::endl ;
  }
#endif
  
#ifdef SPLIT_TEST
  {
    int size = 50 ;
    float position = 0.25 ;

    Bezier sub[2] ;
    b.split(position, sub[0], sub[1]) ;

    std::vector<Point> curve0 ;
    std::vector<Point> curve1 ;

    for(int i = 0; i < size; ++i) {
      curve0.emplace_back(sub[0].interpolate((float) i / (float) (size-1))) ;
      curve1.emplace_back(sub[1].interpolate((float) i / (float) (size-1))) ;
    }

    //plot
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;
    SVG::Element* segment ;

    //split control points
    for(int i =0; i < 4; ++i) {
      SVG::Element* p = plot_point(grp, sub[0].cp[i], 2) ;
      p->set_attr("fill", "red") ;
    }

    segment = plot_segment(grp, sub[0].cp[0], sub[0].cp[1]) ;
    segment->set_attr("stroke", "red") ;
    segment = plot_segment(grp, sub[0].cp[2], sub[0].cp[3]) ;
    segment->set_attr("stroke", "red") ;

    for(int i = 0; i < 4; ++i) {
      SVG::Element* p = plot_point(grp, sub[1].cp[i], 2) ;
      p->set_attr("fill", "blue") ;
    }

    segment = plot_segment(grp, sub[1].cp[0], sub[1].cp[1]) ;
    segment->set_attr("stroke", "blue") ;
    segment = plot_segment(grp, sub[1].cp[2], sub[1].cp[3]) ;
    segment->set_attr("stroke", "blue") ;

    //curve points
    for(const Point& p : curve0) {
      SVG::Element* c = plot_point(grp, p) ;
      c->set_attr("fill", "red") ;
      c->set_attr("stroke", "none") ;
    }
    for(const Point& p : curve1) {
      SVG::Element* c = plot_point(grp, p) ;
      c->set_attr("fill", "blue") ;
      c->set_attr("stroke", "none") ;
    }

    //export the final svg
    svg.autoscale() ;
    std::ofstream file("split.svg") ;
    file << std::string(svg) ;

    std::cout << "split test generated in split.svg" << std::endl ;
  }
#endif

#ifdef SUP_TEST
  {
    Point q(1.5, 3., 0.) ;
    float d = b.sup_distance(q) ;

    //plot
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    //control points
    for(int i =0; i < 4; ++i) {
      SVG::Element* p = plot_point(grp, b.cp[i], 2) ;
      p->set_attr("fill", "black") ;
    }

    plot_segment(grp, b.cp[0], b.cp[1]) ;
    plot_segment(grp, b.cp[2], b.cp[3]) ;

    //curve points
    for(const Point& p : curve) {
      SVG::Element* c = plot_point(grp, p) ;
      c->set_attr("fill", "black") ;
      c->set_attr("stroke", "none") ;
    }

    //query point
    SVG::Element* query = plot_point(grp, q, 2) ;
    query->set_attr("fill", "black") ;

    //radius
    SVG::Element* circle = plot_circle(grp, q, d) ;

    //export the final svg
    svg.autoscale() ;
    std::ofstream file("sup_distance.svg") ;
    file << std::string(svg) ;

    std::cout << "sup_distance test generated in sup_distance.svg" << std::endl ;
  }
#endif

#ifdef INF_TEST
  {
    Point q(1.5, 3., 0.) ;
    float d = b.inf_distance(q) ;

    //plot
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    //control points
    for(int i =0; i < 4; ++i) {
      SVG::Element* p = plot_point(grp, b.cp[i], 2) ;
      p->set_attr("fill", "black") ;
    }

    plot_segment(grp, b.cp[0], b.cp[1]) ;
    plot_segment(grp, b.cp[2], b.cp[3]) ;

    //curve points
    for(const Point& p : curve) {
      SVG::Element* c = plot_point(grp, p) ;
      c->set_attr("fill", "black") ;
      c->set_attr("stroke", "none") ;
    }

    //query point
    SVG::Element* query = plot_point(grp, q, 2) ;
    query->set_attr("fill", "black") ;

    //radius
    SVG::Element* circle = plot_circle(grp, q, d) ;

    //export the final svg
    svg.autoscale() ;
    std::ofstream file("inf_distance.svg") ;
    file << std::string(svg) ;

    std::cout << "inf_distance test generated in inf_distance.svg" << std::endl ;
  }
#endif

#ifdef NEAREST_TEST
  {
    std::vector<Point> queries ;
    std::vector<float> nn ;

    int query_size = 4000 ;
    float error = 1e-5 ;

    Point center = 0.5 * (b.cp[0] + b.cp[3]) ;
    float radius = b.sup_distance(center) ;

    std::default_random_engine alea ;
    std::normal_distribution<float> normal_coord ;
    std::uniform_real_distribution<float> rand_radius(0,1) ;
    for(int i = 0; i < query_size; ++i) {
      Vector q(
          normal_coord(alea),
          normal_coord(alea),
          0
          ) ;
      queries.emplace_back(center + 1.5 * radius * sqrt(rand_radius(alea)) * normalize(q)) ;
    }

    for(int i = 0; i < query_size; ++i) {
      nn.push_back(b.nearest(queries[i])) ;
    }

    //plot
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    for(int i = 0; i < query_size; ++i) {
      //query point
      SVG::Element* query = plot_point(grp, queries[i], 2) ;
      query->set_attr("fill", "black") ;

      //segment
      Point nearest = b.interpolate(nn[i]) ;
      SVG::Element* s = plot_segment(grp, queries[i], nearest) ;
      s->set_attr("stroke-width", "1") ;
    }

    //export the final svg
    svg.autoscale() ;
    std::ofstream file("nearest.svg") ;
    file << std::string(svg) ;

    std::cout << "nearest test generated in nearest.svg" << std::endl ;
  }
#endif

#ifdef CAROT_TEST
  {
    std::vector<Point> queries ;
    std::vector<float> nn ;
    std::vector<float> dn ;

    float r0 = 0.5 ;
    float r1 = 0.1 ;

    Hair carot(b, r0, r1) ;

    int query_size = 4000 ;
    float error = 1e-5 ;

    Point center = 0.5 * (carot.core.cp[0] + carot.core.cp[3]) ;
    float radius = carot.core.sup_distance(center) ;

    std::default_random_engine alea ;
    std::normal_distribution<float> normal_coord ;
    std::uniform_real_distribution<float> rand_radius(0,1) ;
    for(int i = 0; i < query_size; ++i) {
      Vector q(
          normal_coord(alea),
          normal_coord(alea),
          0
          ) ;
      queries.emplace_back(center + 1.5 * radius * sqrt(rand_radius(alea)) * normalize(q)) ;
    }

    for(int i = 0; i < query_size; ++i) {
      nn.push_back(carot.nearest(queries[i], error)) ;
    }

    //plot
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    for(int i = 0; i < query_size; ++i) {
      //query point
      SVG::Element* query = plot_point(grp, queries[i], 2) ;

      //reference core point
      Point core_pt = carot.core.interpolate(nn[i]) ;
      float radius = carot.radius(nn[i]) ;
      Point nearest = core_pt + radius * normalize(queries[i] - core_pt) ;

      SVG::Element* s = plot_segment(grp, queries[i], nearest) ;
      s->set_attr("stroke-width", "1") ;

      if(distance(core_pt, queries[i]) > radius) {
        query->set_attr("fill", "red") ;
        s->set_attr("stroke", "red") ;
      } else {
        query->set_attr("fill", "blue") ;
        s->set_attr("stroke", "none") ;
      }
    }

    //export the final svg
    svg.autoscale() ;
    std::ofstream file("carot.svg") ;
    file << std::string(svg) ;

    std::cout << "carot test generated in carot.svg" << std::endl ;
  }
#endif

  return 0 ;
}
