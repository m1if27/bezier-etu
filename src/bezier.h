#include "vec.h"

#include <array>
#include <vector>

struct Bezier {

  Bezier() {} ;

  Bezier(const Point& p0, const Point& p1, const Point& p2, const Point& p3) ;
  
  Point interpolate(float t) ;

  void split(float t, Bezier& first, Bezier& second) ;

  float sup_distance(const Point& q) ;
  float inf_distance(const Point& q) ;

  float nearest(const Point& q, float precision = 1e-5) ;

  std::array<Point, 4> cp ;
} ;

struct Hair {

  Hair() {} ;

  Hair(const Bezier& b, float r0, float r1) ;

  float radius(float t) ;

  void split(float t, Hair& first, Hair& second) ;

  float nearest(const Point& q, float precision = 1e-5) ;

  Bezier core ;
  std::array<float, 2> r ;
} ;
