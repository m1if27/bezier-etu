#include "bezier.h"

#include <algorithm>
#include <vector>
#include <iostream>

Bezier::Bezier(
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3
    ) : cp{p0, p1, p2, p3} {}

Point Bezier::interpolate(float t) {

  //remplacer par votre code
  
  return Point() ;
}

void Bezier::split(float t, Bezier& first, Bezier& second) {

  //remplacer par votre code

}

float Bezier::sup_distance(const Point& q) {

  //remplacer par votre code

  return 0 ;
}

float Bezier::inf_distance(const Point& q) {

  //remplacer par votre code
  
  return 0 ;

}

float Bezier::nearest(const Point& q, float precision) {

  //remplacer par votre code

  return 0 ;
}

Hair::Hair(const Bezier& b, float r0, float r1) : core(b), r{r0, r1} {}

float Hair::radius(float t) {
  return (1-t)*r[0] + t*r[1] ;
}

void Hair::split(float t, Hair& first, Hair& second) {
  float rmid = radius(t) ;

  core.split(t, first.core, second.core) ;
  first.r = {r[0], rmid} ;
  second.r = {rmid, r[1]} ;
}

float Hair::nearest(const Point& q, float precision) {

  //remplacer par votre code

  return 0 ;
}
